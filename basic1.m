clear all;

n=[-20:30];
%signal one
imp=zeros(1,length(n));
imp(n==0)=1;
stem(n,imp,'fill');
title('Impulase Signal');
xlabel('sample number');
ylabel('Amplutude');
grid on;

%signal two
stp=zeros(1,length(n));
stp(n>=0)=1;
figure;
stem(n,stp,'fill');
title('stp Signal');
xlabel('sample number');
ylabel('Amplutudes');
grid on;
%signal three
rmp=zeros(1,length(n));
rmp(n>=0)= n(n>=0);
figure;
stem(n,rmp,'fill','r--');
title('rmp Signal');
xlabel('sample number');ylabel('Amplutudes');
grid on;
