import matplotlib.pyplot as plt
import numpy as np

plt.close('all')
n=np.arange(-10,10)

l=np.size(n)
ip=np.zeros(l)
ind=np.where(n==0)
ip[ind]=1
plt.stem(n,ip)
plt.title('Impulse signal')
plt.xlabel('sample Number')
plt.ylabel('amplututod')
plt.grid(True)


ipx=np.zeros(l)
ind=np.where(n>=0)
ipx[ind]=1
plt.figure(2)
plt.stem(n,ipx,'r--')
plt.title('Impulse signal')
plt.xlabel('sample Number')
plt.ylabel('amplututod')
plt.grid(True)

ipn=np.zeros(l)
ind=np.where(n==0)
ipn[ind]=n[ind]
plt.figure(3)

plt.stem(n,ipn,'g--')
plt.title('Impulse signal')
plt.xlabel('sample Number')
plt.ylabel('amplututod')
plt.grid(True)